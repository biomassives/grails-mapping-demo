package gapes

class GmapController {

    def index = {
    }

    def show = {
        def addr = []
        addr << params.addressLine << params.city << params.state << params.zipCode << params.country
        //println("addressline=" + address.join(','))
        [address:addr.join(',')]
    }
}
