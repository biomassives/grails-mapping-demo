<html>
<head>
<meta name="viewport" content="initial-scale=1.0, user-scalable=no" />

<script type="text/javascript">

  function initialize() {
    codeAddress("${address}");
  }

  var geocoder = new google.maps.Geocoder();
  var map;

  function codeAddress(data, addressid) {
    geocoder.geocode( { address: data }, function(results, status) {
      if (status == google.maps.GeocoderStatus.OK && results.length) {

        if (status != google.maps.GeocoderStatus.ZERO_RESULTS) {
          var latlng = results[0].geometry.location;

          var myOptions = {
            zoom: 12,
            center: latlng,
            mapTypeId: google.maps.MapTypeId.ROADMAP
          }

          map = new google.maps.Map(document.getElementById("map_canvas"), myOptions);
          //map.set_center(results[0].geometry.location);
          var marker = new google.maps.Marker({
              position: latlng,
              map: map
          });
        }
      } else {
        alert("Geocode was unsuccessful due to: " + status);
      }
    });
  }
</script>
</head>
<body>
  <div id="map_canvas" style="width:100%; height:100%"></div>
  <script type="text/javascript">
    initialize();
  </script>
</body>
</html>