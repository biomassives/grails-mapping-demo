<html>
    <head>
        <title>Welcome to Grails</title>
	<meta name="layout" content="main" />
        <script>
          function showMap() {
            $.post("/googlemapv3/gmap/show",
              {
                addressLine:$('#addressLine').val(),
                city:$('#city').val(),
                state:$('#state').val(),
                zipCode:$('#zipCode').val(),
                country:$('#country').val()
              },
              function(data) {
                $('#map_div').html(data);
              }
            );
          }
        </script>
    </head>
    <body>
      <table style="width:100%;height:100%;border-collapse:collapse;border:1px solid black">
        <tbody>
          <tr>
            <td>
                  <div class="dialog">
                      <table>
                          <tbody>

                              <tr class="prop">
                                  <td valign="top" class="name">
                                      <label for="addressLine1">Address Line:</label>
                                  </td>
                                  <td valign="top">
                                      <input type="text" id="addressLine" name="addressLine" value=""/>
                                  </td>
                              </tr>

                              <tr class="prop">
                                  <td valign="top" class="name">
                                      <label for="city">City:</label>
                                  </td>
                                  <td valign="top">
                                      <input type="text" id="city" name="city" value="Hoffman Estates"/>
                                  </td>
                              </tr>

                              <tr class="prop">
                                  <td valign="top" class="name">
                                      <label for="state">State:</label>
                                  </td>
                                  <td valign="top">
                                      <input type="text" id="state" name="state" value="IL"/>
                                  </td>
                              </tr>

                              <tr class="prop">
                                  <td valign="top" class="name">
                                      <label for="zipCode">Zip Code:</label>
                                  </td>
                                  <td valign="top">
                                      <input type="text" id="zipCode" name="zipCode" value=""/>
                                  </td>
                              </tr>

                              <tr class="prop">
                                  <td valign="top" class="name">
                                      <label for="country">Country:</label>
                                  </td>
                                  <td valign="top">
                                      <input type="text" id="country" name="country" value="US"/>
                                  </td>
                              </tr>

                          </tbody>
                      </table>
                  </div>
                  <div>
                      <span><input type="submit" value="Show Map" onclick="showMap()" /></span>
                  </div>

            </td>
            <td style="width:100%;height:100%;padding:0;border:1px solid black">
              <div id="map_div"/>
            </td>
          </tr>
        </tbody>
      </table>

    </body>
</html>